#!/bin/bash
script_path=`dirname $script`
cd $script_path

pathrunjob=/home/haint/TFIDF
timesleep=10m

for (( ; ; ));
do
  hour=`date +"%H"`
  cd $pathrunjob
  java -cp TFIDF-2.1.1-SNAPSHOT-jar-with-dependencies.jar:dist/*:*  app.Token
  echo -e "Current time: `date`"
  echo -e "Sleep $timesleep ..."
  sleep $timesleep
done
