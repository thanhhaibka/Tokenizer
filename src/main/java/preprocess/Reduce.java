package preprocess;

import app.VCTokenizer;
import connectDB.Cassandra;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import stemmer.StopWords;

import java.io.IOException;

/**
 * Created by pc on 20/10/2016.
 */
public class Reduce extends Reducer<Text, Text, Text, Text> {

    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        String newsid = key.toString();
        String sentences= Cassandra.getInstance().getTextArticle(newsid);
        String tags= Cassandra.getInstance().getTags(newsid);
        String content= getContent(sentences, tags);
        context.write(new Text(newsid), new Text(content));
    }

    private String getContent(String sentences, String tags) {
        String content = "";
        sentences = sentences.replaceAll("\\<.*?>", " ").replaceAll("\\[.*?]", " ");
        if (tags != null) {
            String[] ts = tags.split(";");
            for (String s1 : ts) {
                String temp1 = s1;
                String temp = s1.replaceAll(" ", "987");
                sentences = sentences.replaceAll(temp1, temp);
            }
            ts = tags.replace(" ", "_").split(";");
            boolean flag = true;
            if (sentences != null) {
                try {
                    String[] var1 = VCTokenizer.getInstance().getSegmenter().segment(sentences).split(" ");
                    for (int var5 = 0; var5 < var1.length; var5++) {
                        String var2 = normalize(var1[var5].replaceAll("987", "_"));
                        if(!StopWords.getInstance().isStopword(var2)){
                            content+=var2+" ";
                        }
                    }
                    for (String s : ts) {
                        s = normalize(s);
                        content+=s+" ";
                    }
                } catch (Exception e) {

                }
            }
        }else{
            if (sentences != null) {
                try {
                    String[] var1 = VCTokenizer.getInstance().getSegmenter().segment(sentences).split(" ");
                    for (int var5 = 0; var5 < var1.length; var5++) {
                        String var2 = normalize(var1[var5]);
                        if(!StopWords.getInstance().isStopword(var2)){
                            content+=var2+" ";
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
        return content;
    }

    private static String normalize(String var1) {
        String var2 = var1.toLowerCase();
        return var2;
    }
}
