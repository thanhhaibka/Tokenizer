package ngram;

/**
 * Created by pc on 13/10/2016.
 */

import connectDB.Cassandra;
import connectDB.ConnectMySQL;
import org.apache.pig.backend.executionengine.ExecException;
import stemmer.StopWords;

import java.io.*;
import java.sql.SQLException;
import java.util.*;

public class NGramWords {
    private static Map<String, Double> m_Words = null;
    public static NGramWords instance= null;

    public static NGramWords getInstance(){
        if(instance== null) instance= new NGramWords();
        return instance;
    }

    public NGramWords(){
        m_Words= getNGramWords("NGrams.txt");
    }

    private static HashSet m_StopChars = null;

    private static Map<String, Double> getNGramWords(String path){
        Map<String, Double> list= new HashMap<>();
        try{
            FileInputStream fileInputStream= new FileInputStream(path);
            BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(fileInputStream));
            String line="";
            double max=0;
            int i=0;
            while ((line=bufferedReader.readLine())!=null){
                line= line.replace(" ","");
                String[] splits= line.split(",");
                for(String s: splits){
                    String[] var1= s.split(":");
                    if(i==0){
                        max= Double.valueOf(var1[1]);
                    }
                    double d= Double.valueOf(var1[1])/max;
                    if(d>=0.001){
                        list.put(var1[0], d);
                    }
                    i=1;
                }
            }
        }catch (IOException e){

        }
        return list;
    }

    public double getNGramValue(String beforeWord, String afterWord) {
        String var1= beforeWord+"+"+afterWord;
        String var2= afterWord+"+"+beforeWord;
        double d= 0;
        if(m_Words.containsKey(var1)){
            d= m_Words.get(var1);
        }else if(m_Words.containsKey(var2)){
            d= m_Words.get(var2);
        }
        return d;
    }

    public boolean getNGram(String beforeWord, String afterWord) {
        String var1= beforeWord+"+"+afterWord;
        double d= 0;
        if(m_Words.containsKey(var1)){
            d= m_Words.get(var1);
        }
        return d!=0;
    }

    public static void main(String args[]) throws SQLException, ClassNotFoundException {
        List<String> ids= ConnectMySQL.getInstance().getAllNews();
        try{
            FileWriter fileWriter= new FileWriter(new File("ids"));
            for (String s: ids){
                fileWriter.write(s+"\n");
            }
            fileWriter.flush();
            fileWriter.close();
        }catch (Exception e){

        }
        System.exit(1);
    }

}
