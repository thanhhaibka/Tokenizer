package ngram.hadoopngram;

/**
 * Created by pc on 07/10/2016.
 */

import connectDB.Cassandra;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import stemmer.StopWords;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class ReduceNG extends Reducer<Text, Text, Text, Text> {

    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        StopWords.getInstance();
        String token = key.toString();
        Map<String, Double> map = getCorr(token);
        for(String s: map.keySet()){
            context.write(new Text(s), new Text(map.get(s)+""));
        }
    }

    public Map<String, Double> getCorrNor(String documentList) {
        Map<String, Double> nGrams = new HashMap<>();
        Map<String, Map<String, Double>> corr = new HashMap<>();
        Map<String, Double> tokens = new HashMap<>();
        Map<String, Double> tokensTemp = new HashMap<>();
        Map<String, Double> nGramsTemp = new HashMap<>();
        try {
            tokensTemp = Cassandra.getInstance().getMapWord(documentList);
            Map<String, Double> tokMap = new HashMap<>();
            for (String s : tokensTemp.keySet()) {
                if (!StopWords.getInstance().isStopword(s)) {
                    tokMap.put(s, tokensTemp.get(s));
                } else {

                }
            }
            for (String s : tokMap.keySet()) {
                if (tokMap.get(s) >= 2) {
                    for (String s1 : tokMap.keySet()) {
                        if (!s.equals(s1)) {
                            double d = tokMap.get(s) * tokMap.get(s1);
                            if (d > 8) {
                                if (tokMap.get(s1) >= 2) {
                                    nGramsTemp.put(s + "+" + s1, d);
                                }
                            }
                        }
                    }
                }
            }
            normalize(nGramsTemp);
        } catch (Exception e) {

        }
        return nGramsTemp;
    }

    public Map<String, Double> getCorr(String document) {
        Map<String, Double> nGrams = new HashMap<>();
        Map<String, Map<String, Double>> corr = new HashMap<>();
        Map<String, Double> tokens = new HashMap<>();
        Map<String, Double> tokensTemp = new HashMap<>();
        Map<String, Double> nGramsTemp = new HashMap<>();
        Map<String, Double> tokMap = new HashMap<>();
        try {
            tokensTemp = Cassandra.getInstance().getMapWord(document);
            for (String s : tokensTemp.keySet()) {
                if (!StopWords.getInstance().isStopword(s)) {
                    tokMap.put(s, tokensTemp.get(s));
                } else {

                }
            }
        } catch (Exception e) {

        }
        normalize(tokMap);
        return tokMap;
    }

    private void normalize(Map<String, Double> map) {
        double sum = 0.0;
        for (String s : map.keySet()) {
            sum += map.get(s);
        }
        for (String s : map.keySet()) {
            map.put(s, map.get(s) / sum);
        }
    }
}
