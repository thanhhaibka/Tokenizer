package ngram.hadoopngram;

/**
 * Created by pc on 07/10/2016.
 */

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;


public class MapNG extends Mapper<LongWritable, Text, Text, Text> {
    private Text value_word = new Text("");
    private Text key_word = new Text("");

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        context.write(new Text(line), new Text());
    }
}