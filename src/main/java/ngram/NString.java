package ngram;

/**
 * Created by pc on 07/10/2016.
 */
public class NString implements Comparable<NString>{
    private String string;
    private double value;

    public NString() {
        value= 0;
    }

    public NString(String string, double value) {
        this.string = string;
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public String toString(){
        return this.getString()+":"+this.getValue();
    }

    @Override
    public int compareTo(NString o) {
        return (this.value< o.value)?-1:((this.value==o.value)?0:1);
    }

    @Override
    public boolean equals(Object o){
        if(o==null||o.getClass()!=this.getClass()) return false;
        NString d= (NString) o;
        if(d.getString().equals(this.string)) return true;
        return false;
    }
}

