package ngram;

import app.VCTokenizer;
import config.Document;
import connectDB.Cassandra;
import connectDB.ConnectMySQL;
import javafx.scene.paint.Stop;
import stemmer.StopWords;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by pc on 07/10/2016.
 */
public class NGramTest {

    public static NGramTest instance = null;

    public static NGramTest getInstance() {
        if (instance == null) instance = new NGramTest();
        return instance;
    }

    public NGramTest() {

    }

    private String normalize(String var1) {
        String var2 = var1.toLowerCase();
        return var2;
    }

    public List<String> getNGrams(List<Document> documentList) {
        List<String> nGrams = new ArrayList<>();
        String sentences;
        ArrayList<ArrayList<String>> tokens = new ArrayList<ArrayList<String>>();
        Map<String, String> mapWords = new HashMap<String, String>();
        for (int i = 0; i < documentList.size(); i++) {
            Document document = documentList.get(i);
            System.err.println(document.getNewsID());
            ArrayList<String> t = new ArrayList<String>();
            sentences = document.getContent().replaceAll("\\<.*?>", " ").replaceAll("\\[.*?]", " "); //remove html
            if (sentences != null) {
                try {
                    String[] var1 = VCTokenizer.getInstance().getSegmenter().segment(sentences).split(" ");
                    for (int var5 = 0; var5 < var1.length - 1; var5++) {
                        String var2 = var1[var5];
                        if (!StopWords.getInstance().isStopword(var2)) {
                            String var3 = var1[var5 + 1];
                            if (!StopWords.getInstance().isStopword(var3)) {
                                String net = var2.replace("_", " ") + " " + var3.replace("_", " ");
                                if (sentences.contains(net)) {
                                    net = var2 + "*" + var3;
                                    nGrams.add(net);
                                }
                            }
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
        return nGrams;
    }

    public Map<String, Double> getNStrings(String newsId) {
        Map<String, Double> tokensTemp = new HashMap<>();
        Map<String, Double> nGramsTemp = new HashMap<>();
//        String sentences = Cassandra.getInstance().getTextArticle(newsId).replaceAll("\\<.*?>", " ").replaceAll("\\[.*?]", " "); //remove html
//        if (sentences != null) {
        try {
            tokensTemp = Cassandra.getInstance().getMapWord(newsId);
//                String[] var1 = VCTokenizer.getInstance().getSegmenter().segment(sentences).split(" ");
            for (String var2 : tokensTemp.keySet()) {
                if (!StopWords.getInstance().isStopword(var2)) {
                    for (String var3 : tokensTemp.keySet()) {
                        if (!var3.equals(var2)) {
                            if (!StopWords.getInstance().isStopword(var3)) {
                                String net = var2 + "987" + var3;
                                nGramsTemp.put(net, tokensTemp.get(var2) * tokensTemp.get(var3));
                            }
                        }
                    }
                }
            }
            double sum = tokensTemp.size() * (tokensTemp.size() - 1);
            for (String s : nGramsTemp.keySet()) {
                nGramsTemp.put(s, nGramsTemp.get(s) * Math.log(nGramsTemp.get(s) / sum));
            }
        } catch (Exception e) {

        }
//        }
        return nGramsTemp;
    }

    public Map<String, Double> getCorrNGram(List<Document> documentList) {
        Map<String, Double> nGrams = new HashMap<>();
        String sentences;
        Map<String, Double> corr = new HashMap<>();
        Map<String, Double> tokens = new HashMap<>();
        for (int i = 0; i < documentList.size(); i++) {
            Map<String, Double> tokensTemp = new HashMap<>();
            Document document = documentList.get(i);
            System.err.println(document.getNewsID());
            sentences = document.getContent().replaceAll("\\<.*?>", " ").replaceAll("\\[.*?]", " "); //remove html
            if (sentences != null) {
                try {
                    String[] var1 = VCTokenizer.getInstance().getSegmenter().segment(sentences).split(" ");
                    for (int var5 = 0; var5 < var1.length; var5++) {
                        String var2 = normalize(var1[var5]);
                        if (!StopWords.getInstance().isStopword(var2)) {
                            if (tokensTemp.containsKey(var2)) {
                                tokensTemp.put(var2, tokensTemp.get(var2) + 1);
                            } else {
                                tokensTemp.put(var2, 1.0);
                            }
                        }
                    }
                    for (int var5 = 0; var5 < var1.length - 1; var5++) {
                        String var2 = normalize(var1[var5]);
                        if (!StopWords.getInstance().isStopword(var2)) {
                            String var3 = normalize(var1[var5 + 1]);
                            if (!StopWords.getInstance().isStopword(var3)) {
                                String net = var2.replace("_", " ") + " " + var3.replace("_", " ");
                                if (sentences.contains(net)) {
                                    net = var2 + "987" + var3;
                                    if (nGrams.containsKey(net)) {
                                        nGrams.put(net, nGrams.get(net) + 1);
                                    } else {
                                        nGrams.put(net, 1.0);
                                    }
                                }
                            }
                        }
                    }
                    for (String s : tokensTemp.keySet()) {
                        if (tokens.containsKey(s)) {
                            tokens.put(s, tokens.get(s) + tokensTemp.get(s));
                        } else {
                            tokens.put(s, tokensTemp.get(s));
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
        normalize(nGrams);
        normalize(tokens);
        System.out.println(nGrams.size());
        for (String s : nGrams.keySet()) {
//            System.out.print(s+" ");
            String[] ab = s.split("987");
            String a = ab[0];
            String b = ab[1];
            double value = nGrams.get(s) * Math.log(nGrams.get(s) / (tokens.get(a) * tokens.get(b)));
            corr.put(s.replace("987", "+"), value);
        }
        return corr;
    }

    public Map<String, Double> getCorrNormaly(List<Document> documentList) {
        Map<String, Double> nGrams = new HashMap<>();
        String sentences;
        Map<String, Double> corr = new HashMap<>();
        Map<String, Double> tokens = new HashMap<>();
        for (int i = 0; i < documentList.size(); i++) {
            Map<String, Double> tokensTemp = new HashMap<>();
            Map<String, Double> nGramsTemp = new HashMap<>();
            Document document = documentList.get(i);
            System.err.println(document.getNewsID());
            sentences = document.getContent().replaceAll("\\<.*?>", " ").replaceAll("\\[.*?]", " "); //remove html
            if (sentences != null) {
                try {
                    String[] var1 = VCTokenizer.getInstance().getSegmenter().segment(sentences).split(" ");
                    for (int var5 = 0; var5 < var1.length; var5++) {
                        String var2 = normalize(var1[var5]);
                        if (!StopWords.getInstance().isStopword(var2)) {
                            if (tokensTemp.containsKey(var2)) {
                                tokensTemp.put(var2, tokensTemp.get(var2) + 1);
                            } else {
                                tokensTemp.put(var2, 1.0);
                            }
                        }
                    }
                    for (int var5 = 0; var5 < var1.length - 1; var5++) {
                        String var2 = normalize(var1[var5]);
                        if (!StopWords.getInstance().isStopword(var2)) {
                            for (int var6 = var5 + 1; var6 < var1.length; var6++) {
                                String var3 = normalize(var1[var6]);
                                if (!var3.equals(var2)) {
                                    if (!StopWords.getInstance().isStopword(var3)) {
                                        String net = var2 + "987" + var3;
                                        nGramsTemp.put(net, tokensTemp.get(var2) * tokensTemp.get(var3));
                                    }
                                }
                            }
                        }
                    }
                    double sum = tokensTemp.size() * (tokensTemp.size() - 1);
                    for (String s : nGramsTemp.keySet()) {
                        nGrams.put(s, nGramsTemp.get(s) * Math.log(nGramsTemp.get(s) / sum));
                    }
                    for (String s : nGramsTemp.keySet()) {
                        if (nGrams.containsKey(s)) {
                            nGrams.put(s, nGrams.get(s) + nGramsTemp.get(s));
                        } else {
                            nGrams.put(s, nGramsTemp.get(s));
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
        normalize(nGrams);
//        normalize(tokens);
        System.out.println(nGrams.size());
        return nGrams;
    }

    public Map<String, Double> getCorrNor(List<String> documentList) {
        Map<String, Double> nGrams = new HashMap<>();
        Map<String, Map<String, Double>> corr= new HashMap<>();
        Map<String, Double> tokens = new HashMap<>();
        for (int i = 0; i < documentList.size(); i++) {
            Map<String, Double> tokensTemp = new HashMap<>();
            Map<String, Double> nGramsTemp = new HashMap<>();
            try {
                tokensTemp = Cassandra.getInstance().getMapWord(documentList.get(i));
                Map<String, Double> tokMap= new HashMap<>();
                for(String s: tokensTemp.keySet()){
                    if(!StopWords.getInstance().isStopword(s)) {
                        tokMap.put(s, tokensTemp.get(s));
                    }else{
//                        System.out.println(s);
                    }
                }
                for (String s : tokMap.keySet()) {
                    if(tokMap.get(s)>=2) {
                        for (String s1 : tokMap.keySet()) {
                            if (!s.equals(s1)) {
                                double d= tokMap.get(s) * tokMap.get(s1);
                                if(d>8) {
                                    if (tokMap.get(s1) >= 2) {
                                        nGramsTemp.put(s + "+" + s1, d);
                                    }
                                }
                            }
                        }
                    }
                }
                double sum = tokMap.size() * (tokMap.size() - 1);
                Map<String, Double> nGramsTmp= new HashMap<>();
                for (String s : nGramsTemp.keySet()) {
                    nGramsTmp.put(s, nGramsTemp.get(s) / sum);
                }
                for (String s : nGramsTmp.keySet()) {
                    if (nGrams.containsKey(s)) {
                        nGrams.put(s, nGrams.get(s) + nGramsTmp.get(s));
                    } else {
                        nGrams.put(s, nGramsTmp.get(s));
                    }
                }
            } catch (Exception e) {

            }
        }
        normalize(nGrams);
        System.out.println(nGrams.size());
        return nGrams;
    }

    public void add(Set<Map<String, Double>> set, String s, double d) {
        boolean flag = false;
        for (Map<String, Double> map : set) {
            if (map.containsKey(s)) {
                map.put(s, map.get(s) + d);
                flag = true;
                break;
            } else {
                if (map.size() < 10000) {
                    map.put(s, d);
                    flag = true;
                    break;
                }
            }
        }
        if (!flag) {
            Map<String, Double> m = new HashMap<>();
            m.put(s, d);
            set.add(m);
        }

    }

    private void normalize(Map<String, Double> map) {
        double sum = 0.0;
        for (String s : map.keySet()) {
            sum += map.get(s);
        }
        for (String s : map.keySet()) {
            map.put(s, map.get(s) / sum);
        }
    }

    public Map<String, Double> getCommonWords(List<Document> documentList) {
        Map<String, Double> tokens = new HashMap<>();
        String sentences;
        for (int i = 0; i < documentList.size(); i++) {
            Document document = documentList.get(i);
            System.err.println(document.getNewsID());
            ArrayList<String> t = new ArrayList<String>();
            sentences = document.getContent().replaceAll("\\<.*?>", " ").replaceAll("\\[.*?]", " "); //remove html
            if (sentences != null) {
                Map<String, Double> tokensTemp = new HashMap<>();
                try {
                    String[] var1 = VCTokenizer.getInstance().getSegmenter().segment(sentences).split(" ");
                    for (int var5 = 0; var5 < var1.length - 1; var5++) {
                        String var2 = normalize(var1[var5]);
                        if (!StopWords.getInstance().isStopword(var2)) {
                            if (tokensTemp.containsKey(var2)) {
                                tokensTemp.put(var2, tokensTemp.get(var2) + 1);
                            } else {
                                tokensTemp.put(var2, 1.0);
                            }
                        }
                    }
                } catch (Exception e) {

                }
                normalize(tokensTemp);
                for (String s : tokensTemp.keySet()) {
                    tokensTemp.put(s, 0 - tokensTemp.get(s));
                }
                for (String s : tokensTemp.keySet()) {
                    if (tokens.containsKey(s)) {
                        tokens.put(s, tokens.get(s) + tokensTemp.get(s));
                    } else {
                        tokens.put(s, tokensTemp.get(s));
                    }
                }
            }
        }
        return tokens;
    }

    public static void main(String args[]) throws SQLException, ClassNotFoundException {
        Cassandra.getInstance();
        VCTokenizer.getInstance();
        long t = System.currentTimeMillis();
        List<NString> nStrings = new ArrayList<>();
        List<String> newsids = ConnectMySQL.getInstance().getNewNewsInNumDay(150);
        System.out.println(newsids.size());
        List<Document> documentList = Cassandra.getInstance().getDocsTDays(newsids);
//        List<Document> documentList = Cassandra.getInstance().getDocs("2885620731906312862", "kenh14.vn", 0, 5);
        System.out.println("time get Cass: " + (System.currentTimeMillis() - t));
        Map<String, Double> mapCorr = NGramTest.getInstance().getCorrNGram(documentList);
        for (String s : mapCorr.keySet()) {
            nStrings.add(new NString(s, mapCorr.get(s)));
        }
        Collections.sort(nStrings, new Comparator<NString>() {
            @Override
            public int compare(NString o1, NString o2) {
                return ((o1.getValue() < o2.getValue()) ? 1 : ((o1.getValue() == o2.getValue()) ? 0 : -1));
            }
        });
        try {
            File file = new File("UnNGrams1.txt");
            FileWriter fw = new FileWriter(file);
            fw.write(nStrings.toString());
            fw.flush();
            fw.close();
        } catch (IOException e) {

        }
//        Map<String, Double> mapCommons= NGramTest.getInstance().getCommonWords(documentList);
//        for (String s : mapCommons.keySet()) {
//            nStrings.add(new NString(s, mapCommons.get(s)));
//        }
//        Collections.sort(nStrings, new Comparator<NString>() {
//            @Override
//            public int compare(NString o1, NString o2) {
//                return ((o1.getValue() < o2.getValue()) ? 1 : ((o1.getValue() == o2.getValue()) ? 0 : -1));
//            }
//        });

        System.out.println("time: " + (System.currentTimeMillis() - t));
        System.exit(1);
    }
}
