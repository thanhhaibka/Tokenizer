package stemmer; /**
 * Created by pc on 25/07/2016.
 */
import edu.udo.cs.wvtool.generic.wordfilter.AbstractStopWordFilter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class StopWords extends AbstractStopWordFilter{
    private static HashSet m_Stopwords = null;
    public static StopWords instance= null;

    public static StopWords getInstance(){
        if(instance== null) instance= new StopWords();
        return instance;
    }

    public StopWords(){

    }

    private static HashSet m_StopChars = null;

    private static List<String> getStopWords(String path){
        List<String> list= new ArrayList<String>();
        try{
            FileInputStream fileInputStream= new FileInputStream(path);
            BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(fileInputStream));
            String line="";
            while ((line=bufferedReader.readLine())!=null){
                String s= line.replace(" ","_");
                list.add(s);
            }
        }catch (IOException e){

        }
        return list;
    }

    private static char[] stopChars= new char[]{'(',')',',','.',';','-','+','=','&',':','�',10,'<',39,
            '>','?', '…','%','“','”','!','"','#','$','{','}','/','*',92, '1','2','3','4','5','6','7','8','9','0','_','[',']'};

    static {
        if (m_Stopwords == null) {
            m_Stopwords = new HashSet();
            List<String> stopW= getStopWords("stoplist.txt");
            for (int i = 0; i < stopW.size(); i++) {
                m_Stopwords.add(stopW.get(i));
            }
        }
    }

    static {
        if (m_StopChars == null){
            m_StopChars= new HashSet();
            for(int i=0; i< stopChars.length; i++){
                m_StopChars.add(stopChars[i]);
            }
        }
    }

    public boolean isStopword(String str) {
        if(str== null) return true;
        else if(str.length()==0) return true;
        else if(str.contains("jpg")) return true;
        else if(str.contains("https")) return true;
        else if(str.startsWith("http")) return true;
        else if(str.contains("rel")) return true;
        else if(str.contains("png")) return true;
        else if(str.contains("=")) return true;
        else if(str.length()==1) return true;
        else if (m_StopChars.contains(str.charAt(0))) return true;
        else return m_Stopwords.contains(str.toLowerCase());
    }

    public static void main(String args[]){
        System.out.println(StopWords.getInstance().isStopword("có"));
//        System.out.println(stopW);
    }

}
