package connectDB;

import java.util.Date;

/**
 * Created by pc on 04/10/2016.
 */
public class News implements Comparable<News>{
    private String newsId;
    private String tags;
    private Date publishDate;

    public News(String newsId, String tags, Date publishDate) {
        this.newsId = newsId;
        this.tags = tags;
        this.publishDate = publishDate;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    @Override
    public int compareTo(News o) {
        return (this.publishDate.compareTo(o.publishDate)<0? 1:(this.publishDate.compareTo(o.publishDate)==0?0:1));
    }
}
