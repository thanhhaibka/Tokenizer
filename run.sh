#!/bin/bash
export PATH="$PATH:/opt/hadoop/bin:"

for (( ; ; ));
do
        hour=$(date +"%H")
        read YYYY MM DD <<<$(date +'%Y %m %d')
        echo "Current Hour:" $hour
        if [ $hour -eq 23 ]; then
        echo "Running ..."
        echo $DD $MM
        if [ $MM -lt 10 ]; then
            hadoop jar TFIDF-2.1.1-SNAPSHOT-jar-with-dependencies.jar hadoop.UpdateCass $DD $DD 0$MM test
        else
            hadoop jar TFIDF-2.1.1-SNAPSHOT-jar-with-dependencies.jar hadoop.UpdateCass $DD $DD $MM test
        fi
	fi
	echo "Everything done!"
    echo "Sleep 30 min"
    sleep 30m
done
